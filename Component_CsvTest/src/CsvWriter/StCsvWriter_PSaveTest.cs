﻿using NUnit.Framework;

using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Component.Csv;

namespace Yamabuki.Component.CsvTest
{
    [TestFixture]
    public class StCsvWriter_PSaveTest
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvWriter = CreateDsCsvWriter("s", rootSystem);
            presenter = new StCsvWriter_P(csvWriter);
            view = new StCsvWriter_VMock();
            presenter.View = view;
            view.HasHeader = true;
            presenter.Save();
        }

        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(csvWriter.HasHeader, true);
        }

        DsRootSystem rootSystem;
        DsCsvWriter csvWriter;
        StCsvWriter_P presenter;
        StCsvWriter_VMock view;
    }
}
