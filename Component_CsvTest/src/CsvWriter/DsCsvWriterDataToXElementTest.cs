﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

using NUnit.Framework;

using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Component.Csv;

namespace Yamabuki.Component.CsvTest
{
    [TestFixture]
    public class DsCsvWriterDataToXElementTest
        : CsvBaseTest
    {
        public DsCsvWriterDataToXElementTest()
        {
            this.SetUp();
        }

        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvWriter = CreateDsCsvWriter("c", rootSystem);
            var e = new XElement("Root",
                new XElement("Value", 234));
            csvWriter.Initialize(e.Elements());
            elementList = csvWriter.DataToXElement();
            hasHeaderE = elementList.ToList().Find(x => x.Name.LocalName == "HasHeader");
            encodingE = elementList.ToList().Find(x => x.Name.LocalName == "Encoding");
            delimiterE = elementList.ToList().Find(x => x.Name.LocalName == "Delimiter");
            quoteE = elementList.ToList().Find(x => x.Name.LocalName == "Quote");
            columnLengthE = elementList.ToList().Find(x => x.Name.LocalName == "ColumnLength");
        }

        [Test]
        public void DataToXElementCount()
        {
            Assert.AreEqual(elementList.Count(), 5);
        }

        [Test]
        public void HasHeaderIsNotNull()
        {
            Assert.IsNotNull(hasHeaderE);
        }
        
        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(hasHeaderE.Value, "false");
        }

        [Test]
        public void EncodingIsNotNull()
        {
            Assert.IsNotNull(encodingE);
        }

        [Test]
        public void EncodingValue()
        {
            Assert.AreEqual(encodingE.Value, Encoding.UTF8.CodePage.ToString());
        }

        [Test]
        public void DelimiterIsNotNull()
        {
            Assert.IsNotNull(delimiterE);
        }

        [Test]
        public void DelimiterValue()
        {
            Assert.AreEqual(delimiterE.Value, ",");
        }

        [Test]
        public void QuoteIsNotNull()
        {
            Assert.IsNotNull(quoteE);
        }

        [Test]
        public void QuoteValue()
        {
            Assert.AreEqual(quoteE.Value, "\"");
        }

        [Test]
        public void ColumnLengthIsNotNull()
        {
            Assert.IsNotNull(columnLengthE);
        }

        [Test]
        public void ColumnLengthValue()
        {
            Assert.AreEqual(columnLengthE.Value, "1");
        }
        
        DsRootSystem rootSystem;
        DsCsvWriter csvWriter;
        IEnumerable<XElement> elementList;
        XElement hasHeaderE;
        XElement encodingE;
        XElement delimiterE;
        XElement quoteE;
        XElement columnLengthE;
    }
}
