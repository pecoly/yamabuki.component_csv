﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Component.Csv;

namespace Yamabuki.Component.CsvTest
{
    [TestFixture]
    public class DsCsvWriterBaseTest
        : CsvBaseTest
    {
        public DsCsvWriterBaseTest()
        {
            this.SetUp();
        }

        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvWriter = CreateDsCsvWriter("c", rootSystem);
        }

        [Test]
        public void HasHeader()
        {
            Assert.AreEqual(csvWriter.HasHeader, false);
        }

        [Test]
        public void ColumnLength()
        {
            Assert.AreEqual(csvWriter.ColumnLength, 1);
        }

        [Test]
        public void EncodingTest()
        {
            Assert.AreEqual(csvWriter.Encoding, Encoding.UTF8);
        }

        [Test]
        public void TypeName()
        {
            Assert.AreEqual(csvWriter.TypeName, "");
        }

        DsRootSystem rootSystem;
        DsCsvWriter csvWriter;
    }
}
