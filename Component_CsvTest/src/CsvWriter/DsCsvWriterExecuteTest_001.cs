﻿using System.Xml.Linq;

using NUnit.Framework;

using Yamabuki.Controller.Executer;
using Yamabuki.Design.Utility;
using Yamabuki.Design.Parser;
using Yamabuki.Utility.Xml;


namespace Yamabuki.Component.CsvTest
{
    [TestFixture]
    public class DsCsvWriterExecuteTest_001
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
        }

        /// <summary>
        /// DsRootSystem
        ///  DsString
        ///  DsCsvReader
        ///  DsTerminator
        ///  
        /// DsString-> 
        ///            DsCsvWriter
        /// DsString-> 
        /// </summary>
        [Test]
        public void Execute()
        {
            this.Collection.ClearAll();

            var r = this.CreateDsRootSystem("r");
            var s0 = this.CreateDsStringMock("s0", r);
            var s1 = this.CreateDsStringMock("s1", r);
            var c = this.CreateDsCsvWriter("c", r);

            DsComponentConnectionUtils.ConnectForData(s0, 0, c, 0);

            DsComponentConnectionUtils.ConnectForData(s1, 0, c, 1);

        }
    }
}
