﻿using System.Xml.Linq;
using System.Text;

using NUnit.Framework;

using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Component.Csv;

namespace Yamabuki.Component.CsvTest
{
    [TestFixture]
    public class DsCsvWriterInitilize_EncodingShiftJIS_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvWriter = CreateDsCsvWriter("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "932"),
                new XElement("HasHeader", "true"),
                new XElement("ColumnLength", "2"));
            csvWriter.Initialize(e.Elements());
        }

        [Test]
        public void EncodingValue()
        {
            Assert.AreEqual(csvWriter.Encoding.CodePage, 932);
        }

        DsRootSystem rootSystem;
        DsCsvWriter csvWriter;
    }

    [TestFixture]
    public class DsCsvWriterInitilize_EncodingDefault_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvWriter = CreateDsCsvWriter("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "-1"),
                new XElement("HasHeader", "true"),
                new XElement("ColumnLength", "2"));
            csvWriter.Initialize(e.Elements());
        }

        [Test]
        public void EncodingValue()
        {
            Assert.AreEqual(csvWriter.Encoding.CodePage, 65001);
        }

        DsRootSystem rootSystem;
        DsCsvWriter csvWriter;
    }

    [TestFixture]
    public class DsCsvWriterInitilize_HasHeaderTrue_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvWriter = CreateDsCsvWriter("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "0"),
                new XElement("HasHeader", "true"),
                new XElement("ColumnLength", "2"));
            csvWriter.Initialize(e.Elements());
        }

        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(csvWriter.HasHeader, true);
        }

        DsRootSystem rootSystem;
        DsCsvWriter csvWriter;
    }
    [TestFixture]
    public class DsCsvWriterInitilize_HasHeaderDefault_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvWriter = CreateDsCsvWriter("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "0"),
                new XElement("HasHeader", "0"),
                new XElement("ColumnLength", "2"));
            csvWriter.Initialize(e.Elements());
        }

        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(csvWriter.HasHeader, false);
        }

        DsRootSystem rootSystem;
        DsCsvWriter csvWriter;
    }
    [TestFixture]
    public class DsCsvWriterInitilize_ColumnLength2_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvWriter = CreateDsCsvWriter("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "0"),
                new XElement("HasHeader", "0"),
                new XElement("ColumnLength", "2"));
            csvWriter.Initialize(e.Elements());
        }

        [Test]
        public void ColumnLengthValue()
        {
            Assert.AreEqual(csvWriter.ColumnLength, 2);
        }

        DsRootSystem rootSystem;
        DsCsvWriter csvWriter;
    }
    [TestFixture]
    public class DsCsvWriterInitilize_ColumnLengthDefault_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvWriter = CreateDsCsvWriter("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "0"),
                new XElement("HasHeader", "0"),
                new XElement("ColumnLength", "x"));
            csvWriter.Initialize(e.Elements());
        }

        [Test]
        public void ColumnLengthValue()
        {
            Assert.AreEqual(csvWriter.ColumnLength, 1);
        }

        DsRootSystem rootSystem;
        DsCsvWriter csvWriter;
    }
}
