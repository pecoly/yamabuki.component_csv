﻿using System.Text;

using NUnit.Framework;

using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Component.Csv;

namespace Yamabuki.Component.CsvTest
{
    [TestFixture]
    public class StCsvWriter_PLoad_Default_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvWriter = CreateDsCsvWriter("s", rootSystem);
            presenter = new StCsvWriter_P(csvWriter);
            view = new StCsvWriter_VMock();
            presenter.View = view;
            presenter.Load();
        }

        [Test]
        public void EncodingValue()
        {
            Assert.AreEqual(view.Encoding.CodePage, Encoding.UTF8.CodePage);
        }

        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(view.HasHeader, false);
        }

        [Test]
        public void ColumnLengthValue()
        {
            Assert.AreEqual(view.ColumnLength, 1);
        }

        [Test]
        public void Initialized()
        {
            Assert.IsTrue(view.Initialized);
        }

        DsRootSystem rootSystem;
        DsCsvWriter csvWriter;
        StCsvWriter_P presenter;
        StCsvWriter_VMock view;
    }

    [TestFixture]
    public class StCsvWriter_PLoad_Encoding_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvWriter = CreateDsCsvWriter("s", rootSystem);
            csvWriter.Encoding = Encoding.UTF7;
            presenter = new StCsvWriter_P(csvWriter);
            view = new StCsvWriter_VMock();
            presenter.View = view;
            presenter.Load();
        }

        [Test]
        public void EncodingValue()
        {
            Assert.AreEqual(view.Encoding.CodePage, Encoding.UTF7.CodePage);
        }

        DsRootSystem rootSystem;
        DsCsvWriter csvWriter;
        StCsvWriter_P presenter;
        StCsvWriter_VMock view;
    }

    [TestFixture]
    public class StCsvWriter_PLoad_HasHeader_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvWriter = CreateDsCsvWriter("s", rootSystem);
            csvWriter.HasHeader = true;
            presenter = new StCsvWriter_P(csvWriter);
            view = new StCsvWriter_VMock();
            presenter.View = view;
            presenter.Load();
        }

        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(view.HasHeader, true);
        }

        DsRootSystem rootSystem;
        DsCsvWriter csvWriter;
        StCsvWriter_P presenter;
        StCsvWriter_VMock view;
    }

    [TestFixture]
    public class StCsvWriter_PLoad_ColumnLength_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvWriter = CreateDsCsvWriter("s", rootSystem);
            csvWriter.ColumnLength = 2;
            csvWriter.AddOutputPort("");
            presenter = new StCsvWriter_P(csvWriter);
            view = new StCsvWriter_VMock();
            presenter.View = view;
            presenter.Load();
        }

        [Test]
        public void ColumnLengthValue()
        {
            Assert.AreEqual(view.ColumnLength, 2);
        }

        DsRootSystem rootSystem;
        DsCsvWriter csvWriter;
        StCsvWriter_P presenter;
        StCsvWriter_VMock view;
    }
}
