﻿using System;
using System.Collections.Generic;

using NUnit.Framework;

using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Component.Csv;

namespace Yamabuki.Component.CsvTest
{
    [TestFixture]
    public class DsCsvWriterSetParameterTest
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvWriter = CreateDsCsvWriter("s", rootSystem);
            var paramList = new Dictionary<String, String>();
            paramList.Add("Encoding", "932");
            paramList.Add("HasHeader", "true");
            paramList.Add("ColumnLength", "2");
            csvWriter.SetParameter(paramList);
        }

        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(csvWriter.HasHeader, true);
        }

        DsRootSystem rootSystem;
        DsCsvWriter csvWriter;
    }
}
