﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using NUnit.Framework;

using ComponentFactory.Krypton.Toolkit;

using Yamabuki.Component.Csv;
using Yamabuki.Window.Core;
using Yamabuki.Test.View;

namespace Yamabuki.Component.CsvTest
{
    [TestFixture]
    public class StCsvWriter_VTest
    {
        private StCsvWriter_V view = new StCsvWriter_V();

        [Test]
        public void StartPosition()
        {
            Assert.AreEqual(view.StartPosition, FormStartPosition.CenterScreen);
        }

        [Test]
        public void BorderStyle()
        {
            Assert.AreEqual(view.FormBorderStyle, FormBorderStyle.FixedSingle);
        }

        [Test]
        public void MaximizeBox()
        {
            Assert.AreEqual(view.MaximizeBox, false);
        }

        [Test]
        public void Control()
        {
            ViewTest.Control(view.Controls);
        }

        [Test]
        public void KryptonButton()
        {
            ViewTest.KryptonButton(view.Controls);
        }
    }
}
