﻿using NUnit.Framework;

using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Component.Csv;

namespace Yamabuki.Component.CsvTest
{
    [TestFixture]
    public class StCsvReader_PSaveTest
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("s", rootSystem);
            presenter = new StCsvReader_P(csvReader);
            view = new StCsvReader_VMock();
            presenter.View = view;
            view.HasHeader = true;
            presenter.Save();
        }

        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(csvReader.HasHeader, true);
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
        StCsvReader_P presenter;
        StCsvReader_VMock view;
    }
}
