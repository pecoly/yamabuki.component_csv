﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

using NUnit.Framework;

using Yamabuki.Component.Csv;
using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Base;
using Yamabuki.Design.Connection;
using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Design.Message;
using Yamabuki.Design.Utility;
using Yamabuki.Window.Core;
using Yamabuki.Test.Component.Mock;

namespace Yamabuki.Component.CsvTest
{
    [TestFixture]
    public class DsCsvReader_DoubleClickOk_NoUpdate_Test
        : CsvBaseTest
    {
        public DsCsvReader_DoubleClickOk_NoUpdate_Test()
        {
            this.SetUp();
        }

        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateTestingDsCsvReader("c", rootSystem);
            message = csvReader.DoubleClick();
            messageCollection = message as MessageCollection;
            if (messageCollection != null) {
                messageList = messageCollection.MessageList.ToList();
            }
        }

        [Test]
        public void MessageIsMessageCollection()
        {
            Assert.IsTrue(message is MessageCollection);
        }

        [Test]
        public void MessageCount()
        {
            Assert.AreEqual(messageList.Count, 2);
        }

        [Test]
        public void Message0IsNullMessage()
        {
            Assert.IsTrue(messageList[0] is NullMessage);
        }

        [Test]
        public void Message1IsUpdateComponentMessage()
        {
            Assert.IsTrue(messageList[1] is UpdateComponentMessage);
        }

        private TestingDsCsvReaderOk CreateTestingDsCsvReader(String name, DsComponentImpl parent)
        {
            var com = new TestingDsCsvReaderOk(FormResult.Ok, Encoding.UTF8, false, 1);
            DsComponentCreationUtils.InitializeComponent(this.Collection, 0, 0, name, parent.Data, com);
            return com;
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
        BaseMessage message;
        MessageCollection messageCollection;
        List<BaseMessage> messageList;
    }

    [TestFixture]
    public class DsCsvReader_DoubleClickOk_HasHeaderFalseToTrue_Test
        : CsvBaseTest
    {
        public DsCsvReader_DoubleClickOk_HasHeaderFalseToTrue_Test()
        {
            this.SetUp();
        }

        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateTestingDsCsvReader("c", rootSystem);
            message = csvReader.DoubleClick();
            messageCollection = message as MessageCollection;
            if (messageCollection != null) {
                messageList = messageCollection.MessageList.ToList();
            }
        }

        [Test]
        public void MessageIsMessageCollection()
        {
            Assert.IsTrue(message is MessageCollection);
        }

        [Test]
        public void MessageCount()
        {
            Assert.AreEqual(messageList.Count, 2);
        }

        [Test]
        public void Message0IsNullMessage()
        {
            Assert.IsTrue(messageList[0] is NullMessage);
        }

        [Test]
        public void Message1IsUpdateComponentMessage()
        {
            Assert.IsTrue(messageList[1] is UpdateComponentMessage);
        }

        private TestingDsCsvReaderOk CreateTestingDsCsvReader(String name, DsComponentImpl parent)
        {
            var com = new TestingDsCsvReaderOk(FormResult.Ok, Encoding.UTF8, true, 1);
            DsComponentCreationUtils.InitializeComponent(this.Collection, 0, 0, name, parent.Data, com);
            return com;
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
        BaseMessage message;
        MessageCollection messageCollection;
        List<BaseMessage> messageList;
    }

    [TestFixture]
    public class DsCsvReader_DoubleClickOk_HasHeaderTrueToFalse_HasConnection_Test
        : CsvBaseTest
    {
        public DsCsvReader_DoubleClickOk_HasHeaderTrueToFalse_HasConnection_Test()
        {
            this.SetUp();
        }

        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateTestingDsCsvReader("c", rootSystem);
            csvReader.AddOutputPort("");
            csvReader.HasHeader = true;
            terminator = CreateDsTerminatorMock("t", rootSystem);
            dataCon = DsComponentConnectionUtils.ConnectForData(
                csvReader, 0, terminator, 0);
            message = csvReader.DoubleClick();
            messageCollection = message as MessageCollection;
            if (messageCollection != null) {
                messageList = messageCollection.MessageList.ToList();
            }
        }

        [Test]
        public void MessageIsMessageCollection()
        {
            Assert.IsTrue(message is MessageCollection);
        }

        [Test]
        public void MessageCount()
        {
            Assert.AreEqual(messageList.Count, 2);
        }

        [Test]
        public void Message0IsRemoveDataConnectionMessage()
        {
            Assert.IsTrue(messageList[0] is RemoveDataConnectionMessage);
        }

        [Test]
        public void Message1IsUpdateComponentMessage()
        {
            Assert.IsTrue(messageList[1] is UpdateComponentMessage);
        }

        private TestingDsCsvReaderOk CreateTestingDsCsvReader(String name, DsComponentImpl parent)
        {
            var com = new TestingDsCsvReaderOk(FormResult.Ok, Encoding.UTF8, false, 1);
            DsComponentCreationUtils.InitializeComponent(this.Collection, 0, 0, name, parent.Data, com);
            return com;
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
        DsTerminatorMock terminator;
        DsDataConnection dataCon;
        BaseMessage message;
        MessageCollection messageCollection;
        List<BaseMessage> messageList;
    }

    [TestFixture]
    public class DsCsvReader_DoubleClickOk_HasHeaderTrueToTrue_HasConnection_Test
        : CsvBaseTest
    {
        public DsCsvReader_DoubleClickOk_HasHeaderTrueToTrue_HasConnection_Test()
        {
            this.SetUp();
        }

        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateTestingDsCsvReader("c", rootSystem);
            csvReader.HasHeader = true;
            terminator = CreateDsTerminatorMock("t", rootSystem);
            dataCon = DsComponentConnectionUtils.ConnectForData(
                csvReader, 0, terminator, 0);
            message = csvReader.DoubleClick();
            messageCollection = message as MessageCollection;
            if (messageCollection != null) {
                messageList = messageCollection.MessageList.ToList();
            }
        }

        [Test]
        public void MessageIsMessageCollection()
        {
            Assert.IsTrue(message is MessageCollection);
        }

        [Test]
        public void MessageCount()
        {
            Assert.AreEqual(messageList.Count, 2);
        }

        [Test]
        public void Message0IsNullMessage()
        {
            Assert.IsTrue(messageList[0] is NullMessage);
        }

        [Test]
        public void Message1IsUpdateComponentMessage()
        {
            Assert.IsTrue(messageList[1] is UpdateComponentMessage);
        }

        private TestingDsCsvReaderOk CreateTestingDsCsvReader(String name, DsComponentImpl parent)
        {
            var com = new TestingDsCsvReaderOk(FormResult.Ok, Encoding.UTF8, true, 1);
            DsComponentCreationUtils.InitializeComponent(this.Collection, 0, 0, name, parent.Data, com);
            return com;
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
        DsTerminatorMock terminator;
        DsDataConnection dataCon;
        BaseMessage message;
        MessageCollection messageCollection;
        List<BaseMessage> messageList;
    }

    [TestFixture]
    public class DsCsvReader_DoubleClickOk_ColumnLength1To2_Test
        : CsvBaseTest
    {
        public DsCsvReader_DoubleClickOk_ColumnLength1To2_Test()
        {
            this.SetUp();
        }

        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateTestingDsCsvReader("c", rootSystem);
            message = csvReader.DoubleClick();
            messageCollection = message as MessageCollection;
            if (messageCollection != null) {
                messageList = messageCollection.MessageList.ToList();
            }
        }

        [Test]
        public void MessageIsMessageCollection()
        {
            Assert.IsTrue(message is MessageCollection);
        }

        [Test]
        public void MessageCount()
        {
            Assert.AreEqual(messageList.Count, 3);
        }

        [Test]
        public void Message0IsNullMessage()
        {
            Assert.IsTrue(messageList[0] is NullMessage);
        }

        [Test]
        public void Message1IsNullMessage()
        {
            Assert.IsTrue(messageList[1] is NullMessage);
        }

        [Test]
        public void Message2IsUpdateComponentMessage()
        {
            Assert.IsTrue(messageList[2] is UpdateComponentMessage);
        }

        private TestingDsCsvReaderOk CreateTestingDsCsvReader(String name, DsComponentImpl parent)
        {
            var com = new TestingDsCsvReaderOk(FormResult.Ok, Encoding.UTF8, false, 2);
            DsComponentCreationUtils.InitializeComponent(this.Collection, 0, 0, name, parent.Data, com);
            return com;
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
        BaseMessage message;
        MessageCollection messageCollection;
        List<BaseMessage> messageList;
    }

    [TestFixture]
    public class DsCsvReader_DoubleClickOk_ColumnLength2To1_HasConnection_Test
        : CsvBaseTest
    {
        public DsCsvReader_DoubleClickOk_ColumnLength2To1_HasConnection_Test()
        {
            this.SetUp();
        }

        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateTestingDsCsvReader("c", rootSystem);
            csvReader.AddOutputPort("");
            csvReader.ColumnLength = 2;
            terminator = CreateDsTerminatorMock("t", rootSystem);
            dataCon = DsComponentConnectionUtils.ConnectForData(
                csvReader, 1, terminator, 0);
            message = csvReader.DoubleClick();
            messageCollection = message as MessageCollection;
            if (messageCollection != null) {
                messageList = messageCollection.MessageList.ToList();
            }
        }

        [Test]
        public void MessageIsMessageCollection()
        {
            Assert.IsTrue(message is MessageCollection);
        }

        [Test]
        public void MessageCount()
        {
            Assert.AreEqual(messageList.Count, 3);
        }

        [Test]
        public void Message0IsNullMessage()
        {
            Assert.IsTrue(messageList[0] is NullMessage);
        }

        [Test]
        public void Message1RemoveDataConnectionMessage()
        {
            Assert.IsTrue(messageList[1] is RemoveDataConnectionMessage);
        }

        [Test]
        public void Message2IsUpdateComponentMessage()
        {
            Assert.IsTrue(messageList[2] is UpdateComponentMessage);
        }

        private TestingDsCsvReaderOk CreateTestingDsCsvReader(String name, DsComponentImpl parent)
        {
            var com = new TestingDsCsvReaderOk(FormResult.Ok, Encoding.UTF8, false, 1);
            DsComponentCreationUtils.InitializeComponent(this.Collection, 0, 0, name, parent.Data, com);
            return com;
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
        DsTerminatorMock terminator;
        DsDataConnection dataCon;
        BaseMessage message;
        MessageCollection messageCollection;
        List<BaseMessage> messageList;
    }

    [TestFixture]
    public class DsCsvReader_DoubleClickCancel_Test
        : CsvBaseTest
    {
        public DsCsvReader_DoubleClickCancel_Test()
        {
            this.SetUp();
        }

        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateTestingDsCsvReader("c", rootSystem);
            message = csvReader.DoubleClick();
        }

        [Test]
        public void MessageIsNull()
        {
            Assert.IsNull(message);
        }

        private TestingDsCsvReaderCancel CreateTestingDsCsvReader(String name, DsComponentImpl parent)
        {
            var com = new TestingDsCsvReaderCancel();
            DsComponentCreationUtils.InitializeComponent(this.Collection, 0, 0, name, parent.Data, com);
            return com;
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
        BaseMessage message;
    }

    public class TestingDsCsvReaderOk
        : DsCsvReader
    {
        public TestingDsCsvReaderOk(FormResult result, Encoding encoding,
            Boolean hasHeader, Int32 columnLength)
        {
            this.result = result;
            this.newEncoding = encoding;
            this.newHasHeader = hasHeader;
            this.newColumnLength = columnLength;
        }

        protected override FormResult ShowDialog()
        {
            this.Encoding = this.newEncoding;
            this.HasHeader = this.newHasHeader;
            this.ColumnLength = this.newColumnLength;
            return this.result;
        }

        private FormResult result;
        private Encoding newEncoding;
        private Boolean newHasHeader;
        private Int32 newColumnLength;
    }

    public class TestingDsCsvReaderCancel
        : DsCsvReader
    {
        protected override FormResult ShowDialog()
        {
            return FormResult.Cancel;
        }
    }
}
