﻿using System.Xml.Linq;
using System.Text;

using NUnit.Framework;

using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Component.Csv;

namespace Yamabuki.Component.CsvTest
{
    [TestFixture]
    public class DsCsvReaderInitilize_EncodingShiftJIS_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "932"),
                new XElement("HasHeader", "true"),
                new XElement("Delimiter", ","),
                new XElement("Quote", "\""),
                new XElement("ColumnLength", "2"));
            csvReader.Initialize(e.Elements());
        }

        [Test]
        public void EncodingValue()
        {
            Assert.AreEqual(csvReader.Encoding.CodePage, 932);
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
    }

    [TestFixture]
    public class DsCsvReaderInitilize_EncodingDefault_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "-1"),
                new XElement("HasHeader", "true"),
                new XElement("Delimiter", ","),
                new XElement("Quote", "\""),
                new XElement("ColumnLength", "2"));
            csvReader.Initialize(e.Elements());
        }

        [Test]
        public void EncodingValue()
        {
            Assert.AreEqual(csvReader.Encoding.CodePage, 65001);
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
    }

    [TestFixture]
    public class DsCsvReaderInitilize_HasHeaderTrue_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "0"),
                new XElement("HasHeader", "true"),
                new XElement("Delimiter", ","),
                new XElement("Quote", "\""),
                new XElement("ColumnLength", "2"));
            csvReader.Initialize(e.Elements());
        }

        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(csvReader.HasHeader, true);
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
    }

    [TestFixture]
    public class DsCsvReaderInitilize_HasHeaderDefault_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "0"),
                new XElement("HasHeader", "0"),
                new XElement("Delimiter", ","),
                new XElement("Quote", "\""),
                new XElement("ColumnLength", "2"));
            csvReader.Initialize(e.Elements());
        }

        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(csvReader.HasHeader, false);
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
    }

    [TestFixture]
    public class DsCsvReaderInitilize_DelimiterTab_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "0"),
                new XElement("HasHeader", "0"),
                new XElement("Delimiter", "\t"),
                new XElement("Quote", "\""),
                new XElement("ColumnLength", "2"));
            csvReader.Initialize(e.Elements());
        }

        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(csvReader.Delimiter, '\t');
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
    }

    [TestFixture]
    public class DsCsvReaderInitilize_DelimiterDefault_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "0"),
                new XElement("HasHeader", "0"),
                new XElement("Quote", "\""),
                new XElement("ColumnLength", "2"));
            csvReader.Initialize(e.Elements());
        }

        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(csvReader.Delimiter, ',');
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
    }

    [TestFixture]
    public class DsCsvReaderInitilize_QuoteSingle_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "0"),
                new XElement("HasHeader", "0"),
                new XElement("Delimiter", "\t"),
                new XElement("Quote", "'"),
                new XElement("ColumnLength", "2"));
            csvReader.Initialize(e.Elements());
        }

        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(csvReader.Quote, '\'');
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
    }

    [TestFixture]
    public class DsCsvReaderInitilize_QuoteDefault_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "0"),
                new XElement("HasHeader", "0"),
                new XElement("Delimiter", "\t"),
                new XElement("ColumnLength", "2"));
            csvReader.Initialize(e.Elements());
        }

        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(csvReader.Quote, '\"');
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
    }

    [TestFixture]
    public class DsCsvReaderInitilize_ColumnLength2_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "0"),
                new XElement("HasHeader", "0"),
                new XElement("Delimiter", ","),
                new XElement("Quote", "\""),
                new XElement("ColumnLength", "2"));
            csvReader.Initialize(e.Elements());
        }

        [Test]
        public void ColumnLengthValue()
        {
            Assert.AreEqual(csvReader.ColumnLength, 2);
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
    }

    [TestFixture]
    public class DsCsvReaderInitilize_ColumnLengthDefault_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("s", rootSystem);
            var e = new XElement("Root",
                new XElement("Encoding", "0"),
                new XElement("HasHeader", "0"),
                new XElement("Delimiter", ","),
                new XElement("Quote", "\""),
                new XElement("ColumnLength", "x"));
            csvReader.Initialize(e.Elements());
        }

        [Test]
        public void ColumnLengthValue()
        {
            Assert.AreEqual(csvReader.ColumnLength, 1);
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
    }
}
