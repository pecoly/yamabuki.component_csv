﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

using NUnit.Framework;

using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Component.Csv;

namespace Yamabuki.Component.CsvTest
{
    [TestFixture]
    public class DsCsvReaderDataToXElementTest
        : CsvBaseTest
    {
        public DsCsvReaderDataToXElementTest()
        {
            this.SetUp();
        }

        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("c", rootSystem);
            var e = new XElement("Root",
                new XElement("Value", 234));
            csvReader.Initialize(e.Elements());
            elementList = csvReader.DataToXElement();
            hasHeaderE = elementList.ToList().Find(x => x.Name.LocalName == "HasHeader");
            encodingE = elementList.ToList().Find(x => x.Name.LocalName == "Encoding");
            delimiterE = elementList.ToList().Find(x => x.Name.LocalName == "Delimiter");
            quoteE = elementList.ToList().Find(x => x.Name.LocalName == "Quote");
            columnLengthE = elementList.ToList().Find(x => x.Name.LocalName == "ColumnLength");
        }

        [Test]
        public void DataToXElementCount()
        {
            Assert.AreEqual(elementList.Count(), 5);
        }

        [Test]
        public void HasHeaderIsNotNull()
        {
            Assert.IsNotNull(hasHeaderE);
        }
        
        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(hasHeaderE.Value, "false");
        }

        [Test]
        public void EncodingIsNotNull()
        {
            Assert.IsNotNull(encodingE);
        }

        [Test]
        public void EncodingValue()
        {
            Assert.AreEqual(encodingE.Value, Encoding.UTF8.CodePage.ToString());
        }

        [Test]
        public void DelimiterIsNotNull()
        {
            Assert.IsNotNull(delimiterE);
        }

        [Test]
        public void DelimiterValue()
        {
            Assert.AreEqual(delimiterE.Value, ",");
        }

        [Test]
        public void QuoteIsNotNull()
        {
            Assert.IsNotNull(quoteE);
        }

        [Test]
        public void QuoteValue()
        {
            Assert.AreEqual(quoteE.Value, "\"");
        }

        [Test]
        public void ColumnLengthIsNotNull()
        {
            Assert.IsNotNull(columnLengthE);
        }

        [Test]
        public void ColumnLengthValue()
        {
            Assert.AreEqual(columnLengthE.Value, "1");
        }
        
        DsRootSystem rootSystem;
        DsCsvReader csvReader;
        IEnumerable<XElement> elementList;
        XElement hasHeaderE;
        XElement encodingE;
        XElement delimiterE;
        XElement quoteE;
        XElement columnLengthE;
    }
}
