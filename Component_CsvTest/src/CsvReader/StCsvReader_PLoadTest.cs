﻿using System.Text;

using NUnit.Framework;

using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Component.Csv;

namespace Yamabuki.Component.CsvTest
{
    [TestFixture]
    public class StCsvReader_PLoad_Default_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("s", rootSystem);
            presenter = new StCsvReader_P(csvReader);
            view = new StCsvReader_VMock();
            presenter.View = view;
            presenter.Load();
        }

        [Test]
        public void EncodingValue()
        {
            Assert.AreEqual(view.Encoding.CodePage, Encoding.UTF8.CodePage);
        }

        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(view.HasHeader, false);
        }

        [Test]
        public void ColumnLengthValue()
        {
            Assert.AreEqual(view.ColumnLength, 1);
        }

        [Test]
        public void Initialized()
        {
            Assert.IsTrue(view.Initialized);
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
        StCsvReader_P presenter;
        StCsvReader_VMock view;
    }

    [TestFixture]
    public class StCsvReader_PLoad_Encoding_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("s", rootSystem);
            csvReader.Encoding = Encoding.UTF7;
            presenter = new StCsvReader_P(csvReader);
            view = new StCsvReader_VMock();
            presenter.View = view;
            presenter.Load();
        }

        [Test]
        public void EncodingValue()
        {
            Assert.AreEqual(view.Encoding.CodePage, Encoding.UTF7.CodePage);
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
        StCsvReader_P presenter;
        StCsvReader_VMock view;
    }

    [TestFixture]
    public class StCsvReader_PLoad_HasHeader_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("s", rootSystem);
            csvReader.HasHeader = true;
            presenter = new StCsvReader_P(csvReader);
            view = new StCsvReader_VMock();
            presenter.View = view;
            presenter.Load();
        }

        [Test]
        public void HasHeaderValue()
        {
            Assert.AreEqual(view.HasHeader, true);
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
        StCsvReader_P presenter;
        StCsvReader_VMock view;
    }

    [TestFixture]
    public class StCsvReader_PLoad_ColumnLength_Test
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("s", rootSystem);
            csvReader.ColumnLength = 2;
            csvReader.AddOutputPort("");
            presenter = new StCsvReader_P(csvReader);
            view = new StCsvReader_VMock();
            presenter.View = view;
            presenter.Load();
        }

        [Test]
        public void ColumnLengthValue()
        {
            Assert.AreEqual(view.ColumnLength, 2);
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
        StCsvReader_P presenter;
        StCsvReader_VMock view;
    }
}
