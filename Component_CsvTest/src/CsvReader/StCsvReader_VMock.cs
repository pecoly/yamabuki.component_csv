﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Yamabuki.Component.Csv;
using Yamabuki.Window.Core;

namespace Yamabuki.Component.CsvTest
{
    public class StCsvReader_VMock
        : StCsvReader_VI
    {
        public void ShowModeless()
        {
        }

        public FormResult ShowModal()
        {
            return FormResult.Ok;
        }

        public void Initialize()
        {
            this.Initialized = true;
        }

        public void CloseForm()
        {
        }

        public void Dispose()
        {
        }

        public Action Form_Load
        {
            set { }
        }

        public Func<Boolean> Form_FormClosing
        {
            set { }
        }

        public Action OkButton_Click { get; set; }

        public Action CancelButton_Click { get; set; }

        public Encoding Encoding { get; set; }

        public Boolean HasHeader { get; set; }

        public Char Delimiter { get; set; }

        public Char Quote { get; set; }

        public Int32 ColumnLength { get; set; }

        public String Message { get; set; }

        public FormResult Result { get; set; }

        public Boolean Initialized { get; set; }
    }
}
