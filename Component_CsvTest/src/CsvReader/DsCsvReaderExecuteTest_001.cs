﻿using System.Xml.Linq;

using NUnit.Framework;

using Yamabuki.Controller.Executer;
using Yamabuki.Design.Utility;
using Yamabuki.Design.Parser;
using Yamabuki.Utility.Xml;


namespace Yamabuki.Component.CsvTest
{
    [TestFixture]
    public class DsCsvReaderExecuteTest_001
        : CsvBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
        }

        /// <summary>
        /// DsRootSystem
        ///  DsString
        ///  DsCsvReader
        ///  DsTerminator
        ///  
        /// DsString-> DsCsvReader -> DsTerminator
        /// </summary>
        [Test]
        public void Execute()
        {
            this.Collection.ClearAll();

            var r = this.CreateDsRootSystem("r");
            var s = this.CreateDsStringMock("s", r);
            var c = this.CreateDsCsvReader("c", r);
            var t = this.CreateDsTerminatorMock("t", r);

            DsComponentConnectionUtils.ConnectForData(s, 0, c, 0);

            DsComponentConnectionUtils.ConnectForData(c, 0, t, 0);

            /*
            //  初期値は""
            Assert.AreEqual(s.Value, "");

            //  プロパティの更新
            s.Value = "c:\\test";
            Assert.AreEqual(s.Value, "c:\\test");

            //  Xml出力
            XElement x = s.GetXElement();
            Assert.AreEqual(XmlUtils.GetElementValue(x.Element("Data"), "Value"), "c:\\test");
            
            //  実行準備
            DsParser.InitializeSequence(this.Collection, r);

            //  実行
            var executer = new CtSingleThreadExecuter(r, r.GetTask(), this.DataManager);
            executer.Execute();
            
            Assert.AreEqual(t.Result, "c:\\test\r\n");
            */
        }
    }
}
