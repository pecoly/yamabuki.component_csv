﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Component.Csv;

namespace Yamabuki.Component.CsvTest
{
    [TestFixture]
    public class DsCsvReaderBaseTest
        : CsvBaseTest
    {
        public DsCsvReaderBaseTest()
        {
            this.SetUp();
        }

        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            csvReader = CreateDsCsvReader("c", rootSystem);
        }

        [Test]
        public void HasHeader()
        {
            Assert.AreEqual(csvReader.HasHeader, false);
        }

        [Test]
        public void ColumnLength()
        {
            Assert.AreEqual(csvReader.ColumnLength, 1);
        }

        [Test]
        public void EncodingTest()
        {
            Assert.AreEqual(csvReader.Encoding, Encoding.UTF8);
        }

        [Test]
        public void TypeName()
        {
            Assert.AreEqual(csvReader.TypeName, "");
        }

        DsRootSystem rootSystem;
        DsCsvReader csvReader;
    }
}
