﻿using System;
using System.IO;
using System.Xml.Linq;

using Yamabuki.Component.Csv;
using Yamabuki.Design;
using Yamabuki.Design.Component.Base;
using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Design.Container;
using Yamabuki.Design.Connection;
using Yamabuki.Design.Utility;
using Yamabuki.Task.Data;
using Yamabuki.Test;
using Yamabuki.Test.Component.Mock;

namespace Yamabuki.Component.CsvTest
{
    public class CsvBaseTest
        : BaseTest
    {
        String nameSpace = "Yamabuki.Component.CsvTest";

        public override void Initialize()
        {
            base.Initialize();

            String project = "Component_CsvTest";
            String name = "yamabuki.component_csv";
            String target = "yamabuki";
            String bin = "bin\\Debug";
            Int32 pos;
            
            String path = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            pos = path.IndexOf(project);
            if (pos != -1)
            {
                bin = path.Substring(pos + project.Length + 1);
            }

            pos = path.IndexOf(target, StringComparison.OrdinalIgnoreCase);
            if (pos == -1)
            {
                throw new DirectoryNotFoundException(path);
            }

            this.SolutionPath = path.Substring(0, pos + target.Length) + "\\" + name;
            this.ProjectPath = this.SolutionPath + "\\" + project;
            this.ExePath = Path.Combine(this.ProjectPath, bin);
        }

        public DsCsvReader CreateDsCsvReader(String name, DsComponentImpl parent)
        {
            return DsComponentCreationUtils.CreateComponent<DsCsvReader>(this.Collection, 0, 0, name, parent);
        }

        public DsCsvWriter CreateDsCsvWriter(String name, DsComponentImpl parent)
        {
            return DsComponentCreationUtils.CreateComponent<DsCsvWriter>(this.Collection, 0, 0, name, parent);
        }

        public String SolutionPath { get; private set; }

        public String ProjectPath { get; private set; }
        
        public override String ExePath { get; set; }
    }
}
