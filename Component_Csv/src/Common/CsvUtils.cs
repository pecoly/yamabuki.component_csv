﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

using Yamabuki.Utility.Cast;
using Yamabuki.Utility.Log;

namespace Yamabuki.Component.Csv
{
    internal static class CsvUtils
    {
        /// <summary>ロガー</summary>
        private static readonly Logger Logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// String型をエンコーディングに変換します。
        /// 変換できないときはUTF8を返却します。
        /// </summary>
        /// <param name="s">変換するコードページの文字列</param>
        /// <returns>変換した値</returns>
        internal static Encoding StringToEncoding(String s)
        {
            var codePage = CastUtils.ToInt32(s, (x) => Encoding.UTF8.CodePage);
            return CastUtils.ToEncoding(
                codePage,
                (x, e) =>
                {
                    Logger.Warn(e.Message, e);
                    return Encoding.UTF8;
                });
        }

        /// <summary>
        /// String型をカラム数に変換します。
        /// 最小値は1です。
        /// 変換できないときは1を返却します。
        /// </summary>
        /// <param name="s">変換する文字列</param>
        /// <returns>変換した値</returns>
        internal static Int32 StringToColumnLength(String s)
        {
            return Math.Max(StringToInt32(s), 1);
        }

        /// <summary>
        /// String型をBoolean型に変換します。
        /// 変換できないときはfalseを返却します。
        /// </summary>
        /// <param name="s">変換する文字列</param>
        /// <returns>変換した値</returns>
        internal static Boolean StringToBoolean(String s)
        {
            return CastUtils.ToBoolean(s, (x) => false);
        }

        internal static Char StringToDelimiter(String s)
        {
            if (String.IsNullOrEmpty(s))
            {
                return ',';
            }

            return s[0];
        }

        internal static Char StringToQuote(String s)
        {
            if (String.IsNullOrEmpty(s))
            {
                return '"';
            }

            return s[0];
        }

        /// <summary>
        /// String型をInt32型に変換します。
        /// 変換できないときは0を返却します。
        /// </summary>
        /// <param name="s">変換する文字列</param>
        /// <returns>変換した値</returns>
        internal static Int32 StringToInt32(String s)
        {
            return CastUtils.ToInt32(s, (x) => 0);
        }

        internal static DataTable CreateEncodingTable()
        {
            var encodingTable = new DataTable();

            encodingTable.Columns.Add(EncodingTable.Name, typeof(String));
            encodingTable.Columns.Add(EncodingTable.Encoding, typeof(Encoding));

            var shiftJis = Encoding.GetEncoding(932);
            var euc = Encoding.GetEncoding(51932);
            var jis = Encoding.GetEncoding(50220);

            var encodingList = new Encoding[]
            {
                shiftJis,
                jis,
                euc,
                Encoding.UTF8,
                Encoding.UTF7
            };

            foreach (var encoding in encodingList)
            {
                encodingTable.Rows.Add(new Object[] { encoding.WebName, encoding });
            }

            return encodingTable;
        }

        internal static DataTable CreateDelimiterTable()
        {
            var delimiterTable = new DataTable();

            delimiterTable.Columns.Add(DelimiterTable.Name, typeof(String));
            delimiterTable.Columns.Add(DelimiterTable.Delimiter, typeof(String));

            delimiterTable.Rows.Add(new Object[] { "半角カンマ", "," });
            delimiterTable.Rows.Add(new Object[] { "タブ", "\t" });

            return delimiterTable;
        }

        internal static DataTable CreateQuoteTable()
        {
            var quoteTable = new DataTable();

            quoteTable.Columns.Add(QuoteTable.Name, typeof(String));
            quoteTable.Columns.Add(QuoteTable.Quote, typeof(String));

            quoteTable.Rows.Add(new Object[] { "ダブルクォーテーション", "\"" });
            quoteTable.Rows.Add(new Object[] { "シングルクォーテーション", "\'" });

            return quoteTable;
        }

        public class EncodingTable
        {
            public const String Name = "Name";
            public const String Encoding = "Encoding";
        }

        public class DelimiterTable
        {
            public const String Name = "Name";
            public const String Delimiter = "Delimiter";
        }

        public class QuoteTable
        {
            public const String Name = "Name";
            public const String Quote = "Quote";
        }
    }
}
