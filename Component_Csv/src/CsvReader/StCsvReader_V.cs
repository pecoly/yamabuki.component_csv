﻿using System;
using System.Data;
using System.Text;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.Csv
{
    internal partial class StCsvReader_V
        : DsEditForm_V, StCsvReader_VI
    {
        private EventHandler okButton_Click;

        private EventHandler cancelButton_Click;

        private DataTable encodingTable = new DataTable();

        private DataTable delimiterTable = new DataTable();

        private DataTable quoteTable = new DataTable();

        public StCsvReader_V()
        {
            this.InitializeComponent();
        }

        public Action OkButton_Click
        {
            set
            {
                this.okButton.Click -= this.okButton_Click;
                this.okButton_Click = (sender, e) => value();
                this.okButton.Click += this.okButton_Click;
            }
        }

        public Action CancelButton_Click
        {
            set
            {
                this.cancelButton.Click -= this.cancelButton_Click;
                this.cancelButton_Click = (sender, e) => value();
                this.cancelButton.Click += this.cancelButton_Click;
            }
        }

        public String Message
        {
            set { this.statusLabel.Text = value; }
        }

        public Encoding Encoding
        {
            get { return this.encodingComboBox.SelectedValue as Encoding; }
            set { this.encodingComboBox.SelectedValue = value; }
        }

        public Boolean HasHeader
        {
            get { return this.hasHeaderCheckButton.Checked; }
            set { this.hasHeaderCheckButton.Checked = value; }
        }

        public Char Delimiter
        {
            get { return ((String)this.delimiterComboBox.SelectedValue)[0]; }
            set { this.delimiterComboBox.SelectedValue = value.ToString(); }
        }

        public Char Quote
        {
            get { return ((String)this.quoteComboBox.SelectedValue)[0]; }
            set { this.quoteComboBox.SelectedValue = value.ToString(); }
        }

        public Int32 ColumnLength
        {
            get { return Decimal.ToInt32(this.columnLengthUpDown.Value); }
            set { this.columnLengthUpDown.Value = value; }
        }

        public override void Initialize()
        {
            this.encodingComboBox.Focus();

            this.InitializeEncodingComboBox();
            this.InitializeDelimiterComboBox();
            this.InitializeQuoteComboBox();
            
            this.UpdateHasHeaderCheckButtonText();

            this.hasHeaderCheckButton.CheckedChanged +=
                (sender, e) => this.UpdateHasHeaderCheckButtonText();
        }

        private void InitializeEncodingComboBox()
        {
            this.encodingTable = CsvUtils.CreateEncodingTable();

            this.encodingComboBox.DataSource = this.encodingTable;
            this.encodingComboBox.DisplayMember = CsvUtils.EncodingTable.Name;
            this.encodingComboBox.ValueMember = CsvUtils.EncodingTable.Encoding;
        }

        private void InitializeDelimiterComboBox()
        {
            this.delimiterTable = CsvUtils.CreateDelimiterTable();

            this.delimiterComboBox.DataSource = this.delimiterTable;
            this.delimiterComboBox.DisplayMember = CsvUtils.DelimiterTable.Name;
            this.delimiterComboBox.ValueMember = CsvUtils.DelimiterTable.Delimiter;
        }

        private void InitializeQuoteComboBox()
        {
            this.quoteTable = CsvUtils.CreateQuoteTable();

            this.quoteComboBox.DataSource = this.quoteTable;
            this.quoteComboBox.DisplayMember = CsvUtils.QuoteTable.Name;
            this.quoteComboBox.ValueMember = CsvUtils.QuoteTable.Quote;
        }

        private void UpdateHasHeaderCheckButtonText()
        {
            if (this.hasHeaderCheckButton.Checked)
            {
                this.hasHeaderCheckButton.Text = "読み込む";
            }
            else
            {
                this.hasHeaderCheckButton.Text = "読み込まない";
            }
        }
    }
}
