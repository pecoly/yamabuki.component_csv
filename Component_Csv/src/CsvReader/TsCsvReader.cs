﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using LumenWorks.Framework.IO.Csv;

using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Utility;
using Yamabuki.Utility.Log;

namespace Yamabuki.Component.Csv
{
    internal class TsCsvReader
        : TsTask_1_N
    {
        /// <summary>ロガー</summary>
        private static readonly Logger Logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        private Encoding encoding;
        
        private Boolean hasHeaders;
        
        private Char delimiter;

        private Char quote;

        private Char escape;

        private Char comment;

        public TsCsvReader(
            String definitionPath,
            TsDataStoreGuid inputDataStoreGuid,
            IEnumerable<TsDataStoreGuid> outputDataStoreGuidList,
            Encoding encoding,
            Boolean hasHeaders,
            Char delimiter,
            Char quote,
            Char escape,
            Char comment)
            : base(definitionPath, inputDataStoreGuid, outputDataStoreGuidList)
        {
            this.encoding = encoding;
            this.hasHeaders = hasHeaders;
            this.delimiter = delimiter;
            this.quote = quote;
            this.escape = escape;
            this.comment = comment;
        }

        protected override void CheckInputDataList(TsDataList inputDataList)
        {
            var inputDataListT = inputDataList as TsDataListT<String>;
            if (inputDataListT == null)
            {
                TsDataTypeCheckUtils.Check(this.DefinitionPath, typeof(String), inputDataList.Type);
            }
        }

        protected override void CreateOutputDataList(out List<TsDataList> outputDataListList)
        {
            outputDataListList = new List<TsDataList>();
            for (var i = 0; i < this.OutputLength; i++)
            {
                outputDataListList.Add(new TsDataListT<String>());
            }
        }

        protected override void ExecuteInternal(TsDataList inputDataList, IEnumerable<TsDataList> outputDataListList)
        {
            var inputDataListT = inputDataList as TsDataListT<String>;

            if (inputDataListT.Count != 1)
            {
                throw new TsInputDataInvalidLengthException(
                    1, this.DefinitionPath, CsvReaderConstants.InputPortIndex_FilePath);
            }   
            
            var outputDataListCount = outputDataListList.Count();
            if (this.hasHeaders)
            {
                outputDataListCount--;
            }

            var filePath = inputDataListT.GetValue(0);
            using (var reader = new CsvReader(
                new StreamReader(filePath, this.encoding),
                this.hasHeaders,
                this.delimiter,
                this.quote,
                this.escape,
                this.comment,
                ValueTrimmingOptions.All))
            {
                List<TsDataListT<String>> outputList = new List<TsDataListT<String>>();
                foreach (var outputDataList in outputDataListList)
                {
                    var outputDataListT = outputDataList as TsDataListT<String>;
                    outputList.Add(outputDataListT);
                }

                reader.MissingFieldAction = MissingFieldAction.ReplaceByEmpty;

                if (reader.FieldCount < outputDataListCount)
                {
                    throw new TsRuntimeException(this.DefinitionPath, "範囲外の列を読み込もうとしました。");
                }

                if (this.hasHeaders)
                {
                    var headerList = new List<String>();
                    for (var i = 0; i < outputDataListCount; i++)
                    {
                        headerList.Add("");
                    }

                    // 先頭1行を読み込みヘッダ領域に追加
                    var headers = reader.GetFieldHeaders();
                    var headerCount = outputDataListCount < headers.Length ? outputDataListCount : headers.Length;
                    for (var i = 0; i < headerCount; i++)
                    {
                        headerList[i] = headers[i];
                    }

                    foreach (var header in headerList)
                    {
                        outputList[0].AddValue(header);
                    }

                    // 2行目以降を読み込み、列ごとにデータ領域に追加
                    while (reader.ReadNextRecord())
                    {
                        for (var i = 0; i < outputDataListCount; i++)
                        {
                            outputList[i + 1].AddValue(reader[i]);
                        }
                    }
                }
                else
                {
                    // 1行目以降を読み込み、列ごとにデータ領域に追加
                    while (reader.ReadNextRecord())
                    {
                        for (var i = 0; i < outputDataListCount; i++)
                        {
                            outputList[i].AddValue(reader[i]);
                        }
                    }
                }
            }
        }

        private struct CsvReaderConstants
        {
            public static readonly Int32 InputPortIndex_FilePath = 0;
        }
    }
}
