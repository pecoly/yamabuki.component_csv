﻿namespace Yamabuki.Component.Csv
{
    partial class StCsvReader_V
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelButton = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.okButton = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.encodingComboBox = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.hasHeaderCheckButton = new ComponentFactory.Krypton.Toolkit.KryptonCheckButton();
            this.kryptonLabel4 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.columnLengthUpDown = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.kryptonLabel5 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.delimiterComboBox = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kryptonLabel6 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.quoteComboBox = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.statusStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.encodingComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.delimiterComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quoteComboBox)).BeginInit();
            this.SuspendLayout();
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(317, 317);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(105, 28);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Values.Text = "キャンセル";
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.Location = new System.Drawing.Point(206, 317);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(105, 28);
            this.okButton.TabIndex = 5;
            this.okButton.Values.Text = "OK";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 349);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(434, 23);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 17;
            // 
            // statusLabel
            // 
            this.statusLabel.ForeColor = System.Drawing.Color.Red;
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(23, 18);
            this.statusLabel.Text = "---";
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.Location = new System.Drawing.Point(12, 155);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.kryptonLabel3.Size = new System.Drawing.Size(78, 20);
            this.kryptonLabel3.TabIndex = 18;
            this.kryptonLabel3.Values.Text = "文字コード : ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.kryptonLabel1);
            this.groupBox1.Font = new System.Drawing.Font("メイリオ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(410, 136);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CSV読み込み";
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(6, 25);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(381, 100);
            this.kryptonLabel1.TabIndex = 3;
            this.kryptonLabel1.Values.Text = "CSVファイルを読み込みます。\r\n「文字コード」にはファイルの文字コードを指定してください。\r\n「区切り文字」には列の区切り文字を指定してください。\r\n「囲み文字" +
    "」には値の囲み文字を指定してください。\r\n「ヘッダ」にチェックすると１行目をヘッダとして読み込みます。\r\n読み込んだデータは全て文字列型となります。";
            // 
            // encodingComboBox
            // 
            this.encodingComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.encodingComboBox.DropDownWidth = 150;
            this.encodingComboBox.Location = new System.Drawing.Point(116, 154);
            this.encodingComboBox.Name = "encodingComboBox";
            this.encodingComboBox.Size = new System.Drawing.Size(150, 21);
            this.encodingComboBox.TabIndex = 0;
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Location = new System.Drawing.Point(12, 235);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.kryptonLabel2.Size = new System.Drawing.Size(54, 20);
            this.kryptonLabel2.TabIndex = 21;
            this.kryptonLabel2.Values.Text = "ヘッダ : ";
            // 
            // hasHeaderCheckButton
            // 
            this.hasHeaderCheckButton.Location = new System.Drawing.Point(116, 235);
            this.hasHeaderCheckButton.Name = "hasHeaderCheckButton";
            this.hasHeaderCheckButton.Size = new System.Drawing.Size(105, 28);
            this.hasHeaderCheckButton.TabIndex = 3;
            this.hasHeaderCheckButton.Values.Text = "読み込まない";
            // 
            // kryptonLabel4
            // 
            this.kryptonLabel4.Location = new System.Drawing.Point(12, 269);
            this.kryptonLabel4.Name = "kryptonLabel4";
            this.kryptonLabel4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.kryptonLabel4.Size = new System.Drawing.Size(91, 20);
            this.kryptonLabel4.TabIndex = 24;
            this.kryptonLabel4.Values.Text = "読み込み列数 : ";
            // 
            // columnLengthUpDown
            // 
            this.columnLengthUpDown.Location = new System.Drawing.Point(116, 269);
            this.columnLengthUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.columnLengthUpDown.Name = "columnLengthUpDown";
            this.columnLengthUpDown.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.columnLengthUpDown.Size = new System.Drawing.Size(90, 22);
            this.columnLengthUpDown.TabIndex = 4;
            this.columnLengthUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnLengthUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // kryptonLabel5
            // 
            this.kryptonLabel5.Location = new System.Drawing.Point(12, 182);
            this.kryptonLabel5.Name = "kryptonLabel5";
            this.kryptonLabel5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.kryptonLabel5.Size = new System.Drawing.Size(78, 20);
            this.kryptonLabel5.TabIndex = 26;
            this.kryptonLabel5.Values.Text = "区切り文字 : ";
            // 
            // delimiterComboBox
            // 
            this.delimiterComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.delimiterComboBox.DropDownWidth = 150;
            this.delimiterComboBox.Location = new System.Drawing.Point(116, 181);
            this.delimiterComboBox.Name = "delimiterComboBox";
            this.delimiterComboBox.Size = new System.Drawing.Size(150, 21);
            this.delimiterComboBox.TabIndex = 1;
            // 
            // kryptonLabel6
            // 
            this.kryptonLabel6.Location = new System.Drawing.Point(12, 209);
            this.kryptonLabel6.Name = "kryptonLabel6";
            this.kryptonLabel6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.kryptonLabel6.Size = new System.Drawing.Size(66, 20);
            this.kryptonLabel6.TabIndex = 28;
            this.kryptonLabel6.Values.Text = "囲み文字 : ";
            // 
            // quoteComboBox
            // 
            this.quoteComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.quoteComboBox.DropDownWidth = 150;
            this.quoteComboBox.Location = new System.Drawing.Point(116, 208);
            this.quoteComboBox.Name = "quoteComboBox";
            this.quoteComboBox.Size = new System.Drawing.Size(192, 21);
            this.quoteComboBox.TabIndex = 2;
            // 
            // StCsvReader_V
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(434, 372);
            this.Controls.Add(this.quoteComboBox);
            this.Controls.Add(this.kryptonLabel6);
            this.Controls.Add(this.delimiterComboBox);
            this.Controls.Add(this.kryptonLabel5);
            this.Controls.Add(this.columnLengthUpDown);
            this.Controls.Add(this.kryptonLabel4);
            this.Controls.Add(this.hasHeaderCheckButton);
            this.Controls.Add(this.kryptonLabel2);
            this.Controls.Add(this.encodingComboBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.kryptonLabel3);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Font = new System.Drawing.Font("メイリオ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "StCsvReader_V";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CSV読み込み";
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.encodingComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.delimiterComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quoteComboBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonButton cancelButton;
        private ComponentFactory.Krypton.Toolkit.KryptonButton okButton;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox encodingComboBox;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckButton hasHeaderCheckButton;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel4;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown columnLengthUpDown;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel5;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox delimiterComboBox;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel6;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox quoteComboBox;
    }
}