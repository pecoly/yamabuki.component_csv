﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.Message;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Utility.Log;
using Yamabuki.Window.Core;

namespace Yamabuki.Component.Csv
{
    public class DsCsvReader
        : DsSimpleComponent_1_N
    {
        private const String HeaderName = "ヘッダ";

        private const String FilePathName = "ファイルパス";

        private const Int32 HeaderIndex = 0;

        private const Char Escape = '\\';

        private const Char Comment = '\0';

        /// <summary>ロガー</summary>
        private static readonly Logger Logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        public override String TypeName
        {
            get { return ""; }
        }

        public override int DefaultWidth
        {
            get { return 180; }
        }

        internal Encoding Encoding { get; set; }

        internal Char Delimiter { get; set; }

        internal Char Quote { get; set; }

        internal Boolean HasHeader { get; set; }

        internal Int32 ColumnLength { get; set; }

        protected override string InputName0
        {
            get { return FilePathName; }
        }

        public override void Initialize(IEnumerable<XElement> data)
        {
            this.Encoding = Encoding.UTF8;

            foreach (var e in data)
            {
                if (e.Name == Property.Encoding)
                {
                    this.Encoding = CsvUtils.StringToEncoding(e.Value);
                }
                else if (e.Name == Property.HasHeader)
                {
                    this.HasHeader = CsvUtils.StringToBoolean(e.Value);
                }
                else if (e.Name == Property.Delimiter)
                {
                    this.Delimiter = CsvUtils.StringToDelimiter(e.Value);
                }
                else if (e.Name == Property.Quote)
                {
                    this.Quote = CsvUtils.StringToQuote(e.Value);
                }
                else if (e.Name == Property.ColumnLength)
                {
                    this.ColumnLength = CsvUtils.StringToColumnLength(e.Value);
                }
            }
        }

        public override BaseMessage DoubleClick()
        {
            var oldHasHeader = this.HasHeader;
            var oldColumnLength = this.ColumnLength;
            var oldE = this.GetXElement();
            var result = this.ShowDialog();

            var isUpdated = result == FormResult.Ok;
            if (!isUpdated)
            {
                return null;
            }

            var messageCollection = new MessageCollection();

            messageCollection.Add(this.UpdateHeaderOutputMessage(oldHasHeader, this.HasHeader));
            foreach (var message in this.UpdateDataOutputMessage(oldColumnLength, this.ColumnLength))
            {
                messageCollection.Add(message);
            }

            messageCollection.Add(new UpdateComponentMessage(oldE, this.GetXElement()));

            return messageCollection;
        }

        public override void SetParameter(IDictionary<String, String> paramList)
        {
            foreach (var kv in paramList)
            {
                if (kv.Key == Property.Encoding)
                {
                    this.Encoding = CsvUtils.StringToEncoding(paramList[Property.Encoding]);
                }
                else if (kv.Key == Property.HasHeader)
                {
                    this.HasHeader = CsvUtils.StringToBoolean(paramList[Property.HasHeader]);
                }
                else if (kv.Key == Property.Delimiter)
                {
                    this.Delimiter = CsvUtils.StringToDelimiter(paramList[Property.Delimiter]);
                }
                else if (kv.Key == Property.Quote)
                {
                    this.Quote = CsvUtils.StringToQuote(paramList[Property.Quote]);
                }
                else if (kv.Key == Property.ColumnLength)
                {
                    this.ColumnLength = CsvUtils.StringToColumnLength(paramList[Property.ColumnLength]);
                }
            }
        }

        public override IEnumerable<XElement> DataToXElement()
        {
            var list = new List<XElement>();
            list.Add(new XElement(Property.Encoding, this.Encoding.CodePage));
            list.Add(new XElement(Property.HasHeader, this.HasHeader));
            list.Add(new XElement(Property.Delimiter, this.Delimiter));
            list.Add(new XElement(Property.Quote, this.Quote));
            list.Add(new XElement(Property.ColumnLength, this.ColumnLength));
            return list;
        }

        internal BaseMessage UpdateHeaderOutputMessage(Boolean oldHasHeader, Boolean newHasHeader)
        {
            if (!oldHasHeader && newHasHeader)
            {
                return this.InsertOutputPort(HeaderIndex, HeaderName);
            }
            else if (oldHasHeader && !newHasHeader)
            {
                return this.RemoveOutputPort(HeaderIndex);
            }

            return new NullMessage();
        }

        internal IEnumerable<BaseMessage> UpdateDataOutputMessage(
            Int32 oldColumnLength, Int32 newColumnLength)
        {
            var messageList = new List<BaseMessage>();

            if (oldColumnLength == newColumnLength)
            {
                return messageList;
            }
            else if (oldColumnLength < newColumnLength)
            {
                // 追加されたとき
                for (var i = 0; i < newColumnLength - oldColumnLength; i++)
                {
                    messageList.Add(this.AddOutputPort(""));
                }
            }
            else if (newColumnLength < oldColumnLength)
            {
                // 削除されたとき
                var index = oldColumnLength - 1;
                for (var i = 0; i < oldColumnLength - newColumnLength; i++)
                {
                    messageList.Add(this.RemoveOutputPort(index));
                    index--;
                }
            }

            return messageList;
        }

        protected override void Initialize_1_N()
        {
            this.Encoding = Encoding.UTF8;
            this.Delimiter = ',';
            this.Quote = '"';

            this.AddOutputPort("");
            this.ColumnLength = 1;
        }

        protected virtual FormResult ShowDialog()
        {
            var presenter = new StCsvReader_P(this);
            using (var view = new StCsvReader_V())
            {
                presenter.View = view;
                view.ShowDialog();
                return view.Result;
            }
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid,
            IEnumerable<TsDataStoreGuid> outputDataStoreGuidListList)
        {
            return new TsCsvReader(
                this.DefinitionPath,
                inputDataStoreGuid,
                outputDataStoreGuidListList,
                this.Encoding,
                this.HasHeader,
                this.Delimiter,
                this.Quote,
                Escape,
                Comment);
        }

        /// <summary>プロパティ</summary>
        private struct Property
        {
            internal const String Encoding = "Encoding";
            internal const String HasHeader = "HasHeader";
            internal const String Delimiter = "Delimiter";
            internal const String Quote = "Quote";
            internal const String ColumnLength = "ColumnLength";
        }
    }
}
