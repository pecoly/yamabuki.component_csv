﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.Csv
{
    internal class StCsvReader_P
        : DsEditForm_P<StCsvReader_VI, DsCsvReader>
    {
        public StCsvReader_P(DsCsvReader com)
            : base(com)
        {
        }

        public override void Load()
        {
            this.View.Initialize();
            this.View.Encoding = this.Component.Encoding;
            this.View.HasHeader = this.Component.HasHeader;
            this.View.Delimiter = this.Component.Delimiter;
            this.View.Quote = this.Component.Quote;
            this.View.ColumnLength = this.Component.ColumnLength;
        }

        public override void Save()
        {
            this.Component.Encoding = this.View.Encoding;
            this.Component.HasHeader = this.View.HasHeader;
            this.Component.Delimiter = this.View.Delimiter;
            this.Component.Quote = this.View.Quote;
            this.Component.ColumnLength = this.View.ColumnLength;
        }
    }
}
