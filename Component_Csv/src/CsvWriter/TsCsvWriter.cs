﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Yamabuki.Component.Csv.Properties;
using Yamabuki.Core;
using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Utility;
using Yamabuki.Utility.Check;

namespace Yamabuki.Component.Csv
{
    internal class TsCsvWriter
        : TsTask_N_0
    {
        private Encoding encoding;
        
        private Boolean hasHeaders;
        
        private Char delimiter;

        private Char quote;

        private Char escape;

        public TsCsvWriter(
            String definitionPath,
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            Encoding encoding,
            Boolean hasHeaders,
            Char delimiter,
            Char quote,
            Char escape)
            : base(definitionPath, inputDataStoreGuidList)
        {
            this.encoding = encoding;
            this.hasHeaders = hasHeaders;
            this.delimiter = delimiter;
            this.quote = quote;
            this.escape = escape;

            this.AddInputDataLengthCheckFunc(0, x => x == 1);
        }

        protected override void CheckInputDataList(IEnumerable<TsDataList> inputDataListList)
        {
            Argument.IsNotNull(inputDataListList, "inputDataListList");

            foreach (var inputDataList in inputDataListList)
            {
                var convertedInputDataList = TsAppContext.AutoCastEnabled ?
                    inputDataList.Convert(typeof(String)) : inputDataList;

                TsDataTypeCheckUtils.Check(this.DefinitionPath, typeof(String), convertedInputDataList.Type);
            }
        }

        protected override void ExecuteInternal(IEnumerable<TsDataList> inputDataListList)
        {
            var inputDataList0 = inputDataListList.ElementAt(0);
            var inputDataListT0 = inputDataList0 as TsDataListT<String>;

            if (inputDataListT0.Count != 1)
            {
                throw new TsInputDataInvalidLengthException(
                    1, this.DefinitionPath, CsvReaderConstants.InputPortIndex_FilePath);
            }

            var filePath = inputDataListT0.GetValue(0);

            this.WriteLog(Core.LogLevel.Info, this.DefinitionPath + " " + filePath + "へ書き込みます。");
            using (var writer = this.CreateStreamWriter(filePath, this.encoding))
            {
                Int32 columnBeginIndex;
                Int32 columnEndIndex;
                if (this.hasHeaders)
                {
                    this.WriteHeader(inputDataListList, writer);
                    columnBeginIndex = 2;
                    columnEndIndex = inputDataListList.Count() - 1;
                }
                else
                {
                    columnBeginIndex = 1;
                    columnEndIndex = inputDataListList.Count() - 1;
                }

                var rowLength = this.GetRowLength(inputDataListList, columnBeginIndex);

                // 入力データを元にテーブル(2次元データ)を作成
                var inputDataTable = this.CreateTable(inputDataListList, columnBeginIndex, columnEndIndex);
                
                for (var rowIndex = 0; rowIndex < rowLength; rowIndex++)
                {
                    var valueList = this.CreateList(inputDataTable, rowIndex);
                    this.Write(writer, valueList);
                }
            }
        }
        
        protected Int32 GetRowLength(IEnumerable<TsDataList> inputDataListList, Int32 beginIndex)
        {
            var rowLength = inputDataListList.ElementAt(beginIndex).Count;
            for (var i = beginIndex + 1; i < inputDataListList.Count(); i++)
            {
                if (rowLength != inputDataListList.ElementAt(i).Count)
                {
                    throw new TsInputDataInvalidLengthException(
                        this.DefinitionPath,
                        Resource.InvalidDataLengthToWrite);
                }
            }

            return rowLength;
        }

        protected virtual StreamWriter CreateStreamWriter(String filePath, Encoding encoding)
        {
            return new StreamWriter(filePath, false, encoding);
        }

        private void WriteHeader(IEnumerable<TsDataList> inputDataListList, StreamWriter writer)
        {
            var headerList = TsAppContext.AutoCastEnabled ?
                inputDataListList.ElementAt(1).Convert(typeof(String)) : inputDataListList.ElementAt(1);
            var headerListT = headerList as TsDataListT<String>;

            if (headerList.Count != inputDataListList.Count() - 2)
            {
                throw new TsInputDataInvalidLengthException(
                    this.DefinitionPath,
                    Resource.ComponentColumnLengthAndHeaderColumnLength);
            }

            this.Write(writer, headerListT.ValueList);
        }

        private List<List<String>> CreateTable(
            IEnumerable<TsDataList> inputDataListList,
            Int32 beginIndex,
            Int32 endIndex)
        {
            var inputDataTable = new List<List<String>>();

            for (var columnIndex = beginIndex; columnIndex <= endIndex; columnIndex++)
            {
                var inputDataList = TsAppContext.AutoCastEnabled ?
                    inputDataListList.ElementAt(columnIndex).Convert(typeof(String)) : inputDataListList.ElementAt(columnIndex);
                var inputDataListT = inputDataList as TsDataListT<String>;
                inputDataTable.Add(inputDataListT.ValueList.ToList());
            }

            return inputDataTable;
        }

        private List<String> CreateList(List<List<String>> dataTable, Int32 rowIndex)
        {
            var dataList = new List<String>();
            for (var columnIndex = 0; columnIndex < dataTable.Count; columnIndex++)
            {
                dataList.Add(dataTable[columnIndex][rowIndex]);
            }

            return dataList;
        }

        private void Write(StreamWriter sr, IEnumerable<String> valueList)
        {
            var i = 0;
            int length = valueList.Count();
            foreach (var value in valueList) 
            {
                var temp = value == null ? "" : value;

                // 囲み文字で囲む必要があるか調べる
                if (temp.IndexOf(this.quote) > -1 ||
                    temp.IndexOf(this.delimiter) > -1 ||
                    temp.IndexOf('\r') > -1 ||
                    temp.IndexOf('\n') > -1 ||
                    temp.StartsWith(" ") || temp.StartsWith("\t") ||
                    temp.EndsWith(" ") || temp.EndsWith("\t"))
                {
                    if (temp.IndexOf(this.quote) > -1)
                    {
                        // 囲み文字を連続した囲み文字に置換
                        temp = temp.Replace(
                            this.quote.ToString(),
                            this.quote.ToString() + this.quote.ToString());
                    }

                    temp = this.quote.ToString() + temp + this.quote.ToString();
                }

                // フィールドを書き込む
                sr.Write(temp);

                // カンマを書き込む
                if (i != length - 1)
                {
                    sr.Write(this.delimiter);
                }

                i++;
            }

            sr.Write("\r\n");
        }

        private struct CsvReaderConstants
        {
            public static readonly Int32 InputPortIndex_FilePath = 0;
        }
    }
}
