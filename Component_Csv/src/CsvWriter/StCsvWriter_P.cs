﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.Csv
{
    internal class StCsvWriter_P
        : DsEditForm_P<StCsvWriter_VI, DsCsvWriter>
    {
        public StCsvWriter_P(DsCsvWriter com)
            : base(com)
        {
        }

        public override void Load()
        {
            this.View.Initialize();
            this.View.Encoding = this.Component.Encoding;
            this.View.HasHeader = this.Component.HasHeader;
            this.View.Delimiter = this.Component.Delimiter;
            this.View.Quote = this.Component.Quote;
            this.View.ColumnLength = this.Component.ColumnLength;
        }

        public override void Save()
        {
            this.Component.Encoding = this.View.Encoding;
            this.Component.HasHeader = this.View.HasHeader;
            this.Component.Delimiter = this.View.Delimiter;
            this.Component.Quote = this.View.Quote;
            this.Component.ColumnLength = this.View.ColumnLength;
        }
    }
}
