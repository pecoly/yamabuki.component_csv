﻿using System;
using System.Text;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.Csv
{
    internal interface StCsvWriter_VI
        : DsEditForm_VI
    {
        Encoding Encoding { get; set; }

        Boolean HasHeader { get; set; }

        Char Delimiter { get; set; }

        Char Quote { get; set; }

        Int32 ColumnLength { get; set; }
    }
}
